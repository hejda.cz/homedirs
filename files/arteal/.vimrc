filetype plugin on
syntax on
set number
set fileformats+=dos
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode
