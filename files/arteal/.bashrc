#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# PS1 COLORS
if [ "$UID" != "0" ]; then
   UID_COLOR='\[\e[0;32m\]'
   UID_BG='\[\e[42m\]'
   UID_CHAR='$'
else
   UID_COLOR='\[\e[0;31m\]'
   UID_BG='\[\e[41m\]'
   UID_CHAR='#'
fi

# If I am in screen, I want to know 'bout it
#if [ "$TERM" == "screen" ]; then
#	PROMPT_COMMAND="echo -n [S]"
#fi 
 __git_branch='`git branch 2> /dev/null | grep -e ^* | sed -E  s/^\\\\\*\ \(.+\)$/\(\\\\\1\)\ /`'
# (not so) Shiny PS1
PS1="${UID_BG}${UID_COLOR}[\u@$(hostname -f) \W  $__git_branch]${UID_CHAR}\[\e[0m\] "
export GOPATH=$HOME/go
PATH=$PATH:/home/arteal/bin:$HOME/go/bin:/usr/local/go/bin

# some timesaving stuff
alias ls='ls -F --color=auto'
alias lss='ls -ls'
alias lsA='ls -lsA'
alias xp='xprop | grep "WM_WINDOW_ROLE\|WM_CLASS" && echo "WM_CLASS(STRING) = \"NAME\", \"CLASS\""'
alias r='sudo -E bash -l'
alias s='ssh -CC'
alias ga='git add '
alias gc='git commit'
alias gd='git diff HEAD .'
alias gp='git pull; git push'
alias gpl='git pull;'
alias gt='git status'
alias pingoogle='ping www.google.com'
alias mysqladm='mysql --defaults-file=/etc/mysql/debian.cnf'
alias ppt='puppet agent -tv'
alias dps='docker ps -a'
alias drm='docker rm'
alias drmi='docker rmi'
alias dim='docker images'
#alias ns_chart='netstat -a | grep ESTABLISHED | awk '{print $5}' | awk -F: '{print $1}' | sort | uniq -c'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# enable bash completion in interactive shells
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# history file 3k lines long
export HISTFILESIZE=9000

# Avoid succesive duplicates in the bash command history.
export HISTCONTROL=ignoredups

#I love vim editor
export EDITOR=vim

#non-X machines
export XDG_CONFIG_HOME=/home/arteal/.config

# tweak firefox somehow
export MOZ_DISABLE_PANGO=1

# Make bash check its window size after a process completes
shopt -s checkwinsize

# Append commands to the bash command history file (~/.bash_history)
# instead of overwriting it.
shopt -s histappend

# Have to work with links(not ment as /usr/bin/links, but as <a> tag), but not yet completed
if [ -n "$DISPLAY" ]; then
	export BROWSER=firefox
else
	export BROWSER=links
fi

if [ -f /etc/openrc ]; then
	source /etc/openrc
fi

# Other timesaving stuff
settitle() {
    printf "\033k$1\033\\"
}
dinspect() {
	local id=$1
	docker exec -t -i $id bash -l
}
#ssh() {
#    settitle "$*"
#    command ssh -CC "$@"
#}
genpasswd() {
        local l=$1
        [ "$l" == "" ] && l=20
        tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs
}
dlogs() {
	local id=$1
	local lines=$2
	[ "$lines" == "" ] && lines=150
	docker logs --tail=$lines -f $id
}

# Some funny stuff from cows
#fortune -a | fmt -80 -s | cowsay -$(shuf -n 1 -e b d g p s t w y) -f $(shuf -n 1 -e $(cowsay -l | tail -n +2)) -n

if [ -f ~/.bashrc.arteal ]; then
    source ~/.bashrc.arteal
fi
